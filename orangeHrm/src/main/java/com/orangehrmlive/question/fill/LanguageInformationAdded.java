package com.orangehrmlive.question.fill;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static com.orangehrmlive.userinterface.qualificationpage.QualificationPage.LANGUAGE_COMMENT_VALIDATION;

public class LanguageInformationAdded implements Question<Boolean> {

    private String languageValidationMessage;

    public LanguageInformationAdded wasValidatedWithCommentMessage(String languageValidationMessage) {
        this.languageValidationMessage = languageValidationMessage;
        return this;
    }

    @Override
    public Boolean answeredBy(Actor actor) {
        return LANGUAGE_COMMENT_VALIDATION.resolveFor(actor).containsText(languageValidationMessage);
    }

    public LanguageInformationAdded is(){
        return this;
    }

    public static LanguageInformationAdded languageInformationAdded(){
        return new LanguageInformationAdded();
    }

}
