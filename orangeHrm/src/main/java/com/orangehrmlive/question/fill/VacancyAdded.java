package com.orangehrmlive.question.fill;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static com.orangehrmlive.userinterface.vacancypage.VacancyPage.VACANCY_NAME_VALIDATION;

public class VacancyAdded implements Question<Boolean> {

    private String vacancyNameValidationMessage;

    public VacancyAdded wasValidatedWithVacancyName(String vacancyNameValidationMessage) {
        this.vacancyNameValidationMessage = vacancyNameValidationMessage;
        return this;
    }

    @Override
    public Boolean answeredBy(Actor actor) {
        return VACANCY_NAME_VALIDATION.resolveFor(actor).containsOnlyText(vacancyNameValidationMessage);
    }

    public VacancyAdded is(){
        return this;
    }

    public static VacancyAdded vacancyAdded(){
        return new VacancyAdded();
    }
}
