package com.orangehrmlive.util;

public class Dictionary {

    private Dictionary() {
        throw new AssertionError("Instantiating utility class.");
    }
    public static final String URL_PROVIDER = "https://www.facebook.com/";
    public static final String USER = "Admin";
    public static final String PASS = "admin123";
    public static final String EMPTY_STRING = "";
    public static final String SPACE_STRING = " ";

    public static final String VACANCY = "Junior Account Assistant";
    public static final String EMPLOY_USER          = "Aaliyah.Haq"             ;
    public static final String LOG_OUT_RESPONSE = "LOGIN Panel";

    public static final String HIRING_MANAGER       = "Melan Peiris"      ;
    public static final String ADD_VACANCY_DONE = "VACANCY ADDED CORRECTLY"     ;
    public static final String ADD_VACANCY_VALIDATION_ERROR
            = "THERE WAS AN ERROR TRYING TO VERIFY ADDED VACANCY"               ;

    public static final String NOT_ADDED_VACANCY_DONE =
            "VACANCY NOT ADDED CHECKED CORRECTLY"                               ;
    public static final String NOT_ADDED_VACANCY_VALIDATION_ERROR
            = "THERE WAS AN ERROR TRYING TO VERIFY THAT NO VACANCY WAS ADDED"   ;

    public static final String MANAGE_QUALIFICATIONS_DONE =
            "QUALIFICATIONS MANAGE CHECKED CORRECTLY"                           ;
    public static final String MANAGE_QUALIFICATIONS_VALIDATION_ERROR
            = "THERE WAS AN ERROR TRYING TO VERIFY QUALIFICATIONS MANAGE"       ;

    public static final String VACANCY_DESCRIPTION = " is needed.";
    public static final String EXPERIENCE_COMMENT = "Experience with ";
    public static final String EXPERIENCE_COMMENT_COMPLEMENT = " as ";
    public static final String UNIVERSITY = "University of ";
    public static final String SPECIALIZATION = "Specialization in ";
    public static final String SKILL_COMMENT = "Really good at programming";
    public static final String LANGUAGE_COMMENT = "Really Fluent";
    public static final String COMPANY = " Company";
    public static final String NAME_TO_USE = "Anderson";


}
