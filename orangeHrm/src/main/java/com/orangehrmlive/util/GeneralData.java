package com.orangehrmlive.util;

import com.github.javafaker.Faker;
import com.orangehrmlive.models.*;

import static com.orangehrmlive.util.Dictionary.*;
import static com.orangehrmlive.util.EnumTimeOut.*;

public class GeneralData {

    private GeneralData() {
        throw new AssertionError("Instantiating utility class.");
    }

    private static Faker faker;

    public static VacancyModel vacancyModelData(){

        faker = new Faker();

        String jobTitle = String.valueOf(ONE.getValue());
        String vacancyName = faker.job().title();
        String numberOfPositions = String.valueOf(faker.number().numberBetween(ONE.getValue(), ONE_HUNDRED.getValue()));
        String description = vacancyName.concat(VACANCY_DESCRIPTION);

        VacancyModel vacancyModel = new VacancyModel();

        vacancyModel.setJobTitle          (jobTitle);
        vacancyModel.setVacancyName       (vacancyName);
        vacancyModel.setHiringManager     (HIRING_MANAGER);
        vacancyModel.setNumberOfPositions (numberOfPositions);
        vacancyModel.setDescription       (description);

        return vacancyModel;
    }

    public static QualificationsModel qualificationsModelData(){

        faker = new Faker();

        String companyName = faker.job().field().concat(COMPANY);
        String jobAtCompany = faker.job().title();
        String yearExperienceFrom = String.valueOf(
                faker.number().numberBetween(NINETEEN_SEVENTY.getValue(), TWO_THOUSAND_AND_FIVE.getValue()));
        String yearExperienceTo = String.valueOf(
                faker.number().numberBetween(TWO_THOUSAND_AND_SIX.getValue(), TWO_THOUSAND_AND_TWENTY_ONE.getValue()));
        String experienceComment =  EXPERIENCE_COMMENT + companyName + EXPERIENCE_COMMENT_COMPLEMENT + jobAtCompany;
        String educationLevel = String.valueOf(ONE.getValue());
        String state = faker.address().state();
        String institute = UNIVERSITY + state;
        String field = faker.job().field();
        String specialization = SPECIALIZATION + field;
        String yearEducation = String.valueOf(
                faker.number().numberBetween(NINETEEN_NINETY.getValue(), TWO_THOUSAND_AND_FIFTEEN.getValue()));
        int yearEducationNumber = Integer.parseInt(yearEducation);
        String educationScore = String.valueOf(faker.number().numberBetween(ONE.getValue(), TEN.getValue()));
        String yearEducationTo = String.valueOf(yearEducationNumber + FIVE.getValue());

        String skill = String.valueOf(ONE.getValue());
        String yearsOfExperience = String.valueOf(faker.number().numberBetween(ONE.getValue(), FIFTY.getValue()));

        String language = String.valueOf(ONE.getValue());
        String languageFluency = String.valueOf(ONE.getValue());
        String languageCompetency = String.valueOf(ONE.getValue());

        QualificationsModel qualificationsModel = new QualificationsModel();

        qualificationsModel.setCompanyNameModel(companyName);
        qualificationsModel.setJobAtCompanyModel(jobAtCompany);
        qualificationsModel.setMonthExperienceFromModel(String.valueOf    (faker.number().numberBetween(ZERO.getValue(),TWELVE.getValue())));
        qualificationsModel.setYearExperienceFromModel(yearExperienceFrom);
        qualificationsModel.setMonthExperienceToModel(String.valueOf    (faker.number().numberBetween(ZERO.getValue(),TWELVE.getValue())));
        qualificationsModel.setYearExperienceToModel(yearExperienceTo);
        qualificationsModel.setExperienceCommentModel(experienceComment);

        qualificationsModel.setEducationLevelModel(educationLevel);
        qualificationsModel.setInstituteModel(institute);
        qualificationsModel.setSpecializationModel(specialization);
        qualificationsModel.setYearEducationModel(yearEducation);
        qualificationsModel.setEducationScoreModel(educationScore);
        qualificationsModel.setMonthEducationStartModel(String.valueOf    (faker.number().numberBetween(ZERO.getValue(),TWELVE.getValue())));
        qualificationsModel.setYearEducationStartModel(yearEducation);
        qualificationsModel.setMonthEducationEndModel(String.valueOf    (faker.number().numberBetween(ZERO.getValue(), TWELVE.getValue())));
        qualificationsModel.setYearEducationEndModel(yearEducationTo);

        qualificationsModel.setSkillModel(skill);
        qualificationsModel.setYearsOfExperienceModel(yearsOfExperience);
        qualificationsModel.setSkillCommentModel(SKILL_COMMENT);

        qualificationsModel.setLanguageModel(language);
        qualificationsModel.setLanguageFluencyModel(languageFluency);
        qualificationsModel.setLanguageCompetencyModel(languageCompetency);
        qualificationsModel.setLanguageCommentModel(LANGUAGE_COMMENT);

        return qualificationsModel;
    }
}
