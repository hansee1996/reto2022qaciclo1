package com.orangehrmlive.task.go;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Scroll;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static com.orangehrmlive.userinterface.myinfointerface.MyInfoInterface.*;
import static com.orangehrmlive.util.EnumTimeOut.TWENTY_TWO;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class GoToMyInfo implements Task {

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                WaitUntil.the           (BTN_MY_INFO,isVisible()).forNoMoreThan(TWENTY_TWO.getValue()).seconds(),
                Scroll.to               (BTN_MY_INFO),
                Click.on                (BTN_MY_INFO)
        );

    }

    public static GoToMyInfo goToMyInfo(){
        return new GoToMyInfo();
    }
}
