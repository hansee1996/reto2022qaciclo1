package com.orangehrmlive.task.go;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Scroll;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static com.orangehrmlive.userinterface.logout.Logout.LOG_OUT;
import static com.orangehrmlive.userinterface.logout.Logout.YOUR_NAME;
import static com.orangehrmlive.util.EnumTimeOut.TWENTY_TWO;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class GoToLogOut implements Task {

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                WaitUntil.the           (YOUR_NAME,isVisible()).forNoMoreThan(TWENTY_TWO.getValue()).seconds(),

                Scroll.to               (YOUR_NAME),
                Click.on                (YOUR_NAME),

                Scroll.to               (LOG_OUT),
                Click.on                (LOG_OUT)
        );

    }

    public static GoToLogOut goToLogOut(){
        return new GoToLogOut();
    }
}
