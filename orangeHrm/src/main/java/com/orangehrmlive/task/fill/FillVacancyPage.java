package com.orangehrmlive.task.fill;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.Scroll;
import net.serenitybdd.screenplay.actions.SelectFromOptions;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static com.orangehrmlive.userinterface.vacancypage.VacancyPage.*;
import static com.orangehrmlive.util.EnumTimeOut.TWENTY_TWO;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class FillVacancyPage implements Task {

    private String jobTitle;
    private String vacancyName;
    private String hiringManager;
    private String numberOfPositions;
    private String description;

    public FillVacancyPage selectingTheJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
        return this;
    }

    public FillVacancyPage typingTheVacancyName(String vacancyName) {
        this.vacancyName = vacancyName;
        return this;
    }

    public FillVacancyPage typingTheHiringManager(String hiringManager) {
        this.hiringManager = hiringManager;
        return this;
    }

    public FillVacancyPage typingTheNumberOfPositions(String numberOfPositions) {
        this.numberOfPositions = numberOfPositions;
        return this;
    }

    public FillVacancyPage typingTheJobDescription(String description) {
        this.description = description;
        return this;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(
                WaitUntil.the   (SELECT_JOB,isVisible()).forNoMoreThan(TWENTY_TWO.getValue()).seconds(),

                Scroll.to       (SELECT_JOB),
                SelectFromOptions.byValue(jobTitle).from(SELECT_JOB),

                Scroll.to       (VACANCY_NAME),
                Enter.theValue  (vacancyName).into(VACANCY_NAME),

                Scroll.to       (HIRING_MANAGER),
                Enter.theValue  (hiringManager).into(HIRING_MANAGER),

                Scroll.to       (NUMBER_OF_POSITIONS),
                Enter.theValue  (numberOfPositions).into(NUMBER_OF_POSITIONS),

                Scroll.to       (JOB_DESCRIPTION),
                Enter.theValue  (description).into(JOB_DESCRIPTION),

                Scroll.to       (VACANCY_SAVE_BTN),
                Click.on        (VACANCY_SAVE_BTN)
        );

    }

    public static FillVacancyPage fillVacancyPage(){
        return new FillVacancyPage();
    }
}
