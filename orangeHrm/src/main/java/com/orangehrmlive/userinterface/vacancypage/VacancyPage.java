package com.orangehrmlive.userinterface.vacancypage;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class VacancyPage extends PageObject {

    public static final Target RECRUITMENT_MENU = Target
            .the("Recruitment menu")
            .located(By.id("menu_recruitment_viewRecruitmentModule"));

    public static final Target VACANCIES = Target
            .the("Vacancies")
            .located(By.id("menu_recruitment_viewJobVacancy"));

    public static final Target ADD_BTN = Target
            .the("Add Button")
            .located(By.id("btnAdd"));

    public static final Target DELETE_BTN = Target
            .the("Delete Button")
            .located(By.id("btnDelete"));

    public static final Target CANCEL_BTN = Target
            .the("Cancel Button")
            .located(By.xpath("//*[@id=\"deleteConfirmation\"]/div[3]/input[2]"));

    public static final Target OK_BTN = Target
            .the("Ok Button")
            .located(By.id("dialogDeleteBtn"));

    public static final Target SELECT_ALL_VACANCIES = Target
            .the("Select All vacancies")
            .located(By.id("ohrmList_chkSelectAll"));

    public static final Target SELECT_JOB = Target
            .the("Select Job Title")
            .located(By.id("addJobVacancy_jobTitle"));

    public static final Target VACANCY_NAME = Target
            .the("Vacancy Name")
            .located(By.id("addJobVacancy_name"));

    public static final Target HIRING_MANAGER = Target
            .the("Hiring Manager")
            .located(By.id("addJobVacancy_hiringManager"));

    public static final Target NUMBER_OF_POSITIONS = Target
            .the("Number of Positions")
            .located(By.id("addJobVacancy_noOfPositions"));

    public static final Target JOB_DESCRIPTION = Target
            .the("Job Description")
            .located(By.id("addJobVacancy_description"));

    public static final Target VACANCY_SAVE_BTN = Target
            .the("Save Button")
            .located(By.id("btnSave"));

    public static final Target VACANCY_BACK_BTN = Target
            .the("Back Button")
            .located(By.id("btnBack"));

    public static final Target VACANCY_SEARCH = Target
            .the("Select Vacancy")
            .located(By.id("vacancySearch_jobVacancy"));

    public static final Target VACANCY_SEARCH_BTN = Target
            .the("Search Button")
            .located(By.id("btnSrch"));

    public static final Target VACANCY_NAME_VALIDATION = Target
            .the("Vacancy Name Validation")
            .located(By.xpath("//*[@id=\"resultTable\"]/tbody/tr/td[2]/a"));

    public static final Target NO_HIRING_MANAGER_NAME_VALIDATION = Target
            .the("No Hiring Manager Selected Validation")
            .located(By.xpath("//*[@for=\"addJobVacancy_hiringManager\" and @class=\"validation-error\"]"));

}
