package com.orangehrmlive.userinterface.logout;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class Logout extends PageObject {

    public static final Target YOUR_NAME = Target
            .the("your name")
            .located(By.id("welcome"));

    public static final Target LOG_OUT = Target
            .the("log out")
            .located(By.xpath("//*[@id=\"welcome-menu\"]/ul/li[3]/a"));

    public static final Target MSG_OK_LOGOUT = Target
            .the("log in ")
            .located(By.id("logInPanelHeading"));

}
