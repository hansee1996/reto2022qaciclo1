# new feature
# Tags: optional

Feature: Edit Staff Data

  Me as a selection analyst
  I want to modify data of the entered personnel
  To keep the information updated

  Scenario: Edit name
    Given I am in the Personal Details tab
    When I edited the person's name
    Then Show confirmation message of successful editing