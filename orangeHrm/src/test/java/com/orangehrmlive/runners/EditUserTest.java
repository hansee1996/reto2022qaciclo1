package com.orangehrmlive.runners;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        features = "src/test/resources/features/orangehrmlive/edit_user.feature",
        glue = "com.orangehrmlive.stepdefinitions",
        snippets = CucumberOptions.SnippetType.CAMELCASE
)
public class EditUserTest {

}
