package com.orangehrmlive.stepdefinitions;

import com.orangehrmlive.exceptions.ValidationTextDoNotMatch;
import com.orangehrmlive.stepdefinitions.setup.SetUp;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.log4j.Logger;

import static com.orangehrmlive.exceptions.ValidationTextDoNotMatch.VALIDATION_DO_NOT_MATCH;
import static com.orangehrmlive.question.edituser.EditUser.editUserValidatedWithNewNameAdd;
import static com.orangehrmlive.task.edit.EditUser.editUser;
import static com.orangehrmlive.task.fill.FillLogIn.fillLogIn;
import static com.orangehrmlive.task.go.GoToMyInfo.goToMyInfo;
import static com.orangehrmlive.task.landingpage.OpenLandingPage.openLandingPage;
import static com.orangehrmlive.userinterface.myinfointerface.MyInfoInterface.FIRST_NAME_DETAILS;
import static com.orangehrmlive.util.Dictionary.*;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.CoreMatchers.equalTo;

public class EditUserStepDefinition extends SetUp {

    private static final Logger LOGGER = Logger.getLogger(EditUserStepDefinition.class);
    private static final String ACTOR_NAME = "Admin";

    @Given("I am in the Personal Details tab")
    public void iAmInThePersonalDetailsTab() {

        try{
            actorSetupTheBrowser(ACTOR_NAME);
            theActorInTheSpotlight().wasAbleTo(
                    openLandingPage(),
                    fillLogIn()
                            .withUserName(USER)
                            .andPassword(PASS),
                    goToMyInfo()
            );
        }catch (Exception exception){
            LOGGER.error(exception.getMessage(), exception);
        }
    }

    @When("I edited the person's name")
    public void iEditedThePersonsName() {

        try{
            theActorInTheSpotlight().attemptsTo(
                    editUser()
                            .useFirstName(NAME_TO_USE)
            );
        }catch (Exception exception){
            LOGGER.error(exception.getMessage(), exception);
        }

    }

    @Then("Show confirmation message of successful editing")
    public void showConfirmationMessageOfSuccessfulEditing() {

        try {
            theActorInTheSpotlight().should(
                    seeThat(
                            editUserValidatedWithNewNameAdd()
                                    .is(), equalTo(true)
                    )
                            .orComplainWith(ValidationTextDoNotMatch.class,
                                    String.format(VALIDATION_DO_NOT_MATCH, compareInWithSystemOutcome()))
            );
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
        }

    }

    private String compareInWithSystemOutcome(){
        return "\n"
                + "Data for test : System outcome" + "\n"
                + NAME_TO_USE + " : "
                + FIRST_NAME_DETAILS.resolveFor(theActorInTheSpotlight()).getText();
    }
}
