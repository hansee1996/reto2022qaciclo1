package com.orangehrmlive.stepdefinitions.setup;

import io.github.bonigarcia.wdm.WebDriverManager;
import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.thucydides.core.ThucydidesSystemProperty;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.util.EnvironmentVariables;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;

import java.util.Locale;
import java.util.Optional;

import static com.google.common.base.StandardSystemProperty.USER_DIR;
import static com.orangehrmlive.util.Log4jValues.LOG4J_LINUX_PROPERTIES_FILE_PATH;
import static com.orangehrmlive.util.Log4jValues.LOG4J_PROPERTIES_FILE_PATH;
import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;

public class SetUp {
    @Managed(options = "--whitelisted-ips=''")
    private WebDriver browser;

    private final Dimension dimension= new Dimension(600,400);

    private EnvironmentVariables environmentVariables;
    //protected static String operativeSystem = System.getProperty("os.name").toLowerCase();
    private static final Logger LOGGER = Logger.getLogger(SetUp.class);

    private void setupBrowser(WebDriver browser){
        soSystem();
        browser.manage().deleteAllCookies();
        //browser.manage().window().setSize(dimension);
        //browser.manage().window().maximize();
    }

    private void setupUser(String name, WebDriver browser) {
        OnStage.setTheStage(new OnlineCast());
        theActorCalled(name).can(BrowseTheWeb.with(browser));
    }

    protected void actorSetupTheBrowser(String actorName) {
        setUpLog4j2();
        setupBrowser(browser);
        setupUser(actorName, browser);
    }

    private void soSystem(){
        Optional<String> browserOptionalName = EnvironmentSpecificConfiguration.from(environmentVariables)
                .getOptionalProperty(ThucydidesSystemProperty.WEBDRIVER_DRIVER);
        String browserName = browserOptionalName.stream().findFirst().orElse("chrome");

        /*System.out.println(EnvironmentSpecificConfiguration.from(environmentVariables)
                .getOptionalProperty(ThucydidesSystemProperty.SERENITY_FEATURES_DIRECTORY));*/
        System.out.println(browserName);

        switch (browserName) {
            case "chrome":
                WebDriverManager.chromedriver().setup();
                break;
            case "firefox":
                WebDriverManager.firefoxdriver().setup();
                break;
            case "safari":
                WebDriverManager.safaridriver().setup();
                break;
            default:
                LOGGER.error("Error choosing webdriver driver as: ".concat(browserName));
        }
     /*   if(operativeSystem.contains("nux")){
            WebDriverManager.chromedriver().linux().setup();
        }else{
            WebDriverManager.chromedriver().win().setup();
        }*/
    }

    protected void setUpLog4j2(){
        String os = System.getProperty("os.name").toLowerCase(Locale.ROOT).substring(0,3);
        if ("lin".equals(os)) {
            PropertyConfigurator.configure(USER_DIR.value()
                    + LOG4J_LINUX_PROPERTIES_FILE_PATH.getValue());
        } else if ("win".equals(os)){
            PropertyConfigurator.configure(USER_DIR.value()
                    + LOG4J_PROPERTIES_FILE_PATH.getValue());
        }
    }
}
