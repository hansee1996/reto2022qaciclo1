package com.orangehrmlive.stepdefinitions;

import com.orangehrmlive.stepdefinitions.setup.SetUp;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.log4j.Logger;

import static com.orangehrmlive.question.logout.LogOut.logOut;
import static com.orangehrmlive.task.fill.FillLogIn.fillLogIn;
import static com.orangehrmlive.task.go.GoToLogOut.goToLogOut;
import static com.orangehrmlive.task.landingpage.OpenLandingPage.openLandingPage;
import static com.orangehrmlive.util.Dictionary.*;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.CoreMatchers.equalTo;

public class LogOutStepDefinition extends SetUp {

    private static final Logger LOGGER = Logger.getLogger(LogOutStepDefinition.class);
    private static final String ACTOR_NAME = "Admin";

    @Given("I login to the orange home page")
    public void iloginToTheOrangeHomePage() {
        try{
            actorSetupTheBrowser(ACTOR_NAME);
            theActorInTheSpotlight().wasAbleTo(
                    openLandingPage(),
                    fillLogIn()
                            .withUserName(USER)
                            .andPassword(PASS)
            );
        }catch (Exception exception){
            LOGGER.error(exception.getMessage(), exception);
        }
    }

    @When("I end my work and log out")
    public void iEndMyWorkAndLogOut() {

        try{
            theActorInTheSpotlight().attemptsTo(
                    goToLogOut()
            );

        }catch (Exception exception){
            LOGGER.error(exception.getMessage(), exception);
        }

    }

    @Then("i should see the login page")
    public void iShouldSeeTheLoginPage() {

        try {
            theActorInTheSpotlight().should(
                    seeThat(
                            logOut()
                                    .is(), equalTo(true)
                    )
            );
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
        }
    }
}
