package com.orangehrmlive.stepdefinitions;

import com.orangehrmlive.models.QualificationsModel;
import com.orangehrmlive.stepdefinitions.setup.SetUp;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.log4j.Logger;

import static com.orangehrmlive.question.fill.EducationInformationAdded.educationInformationAdded;
import static com.orangehrmlive.question.fill.LanguageInformationAdded.languageInformationAdded;
import static com.orangehrmlive.question.fill.SkillInformationAdded.skillInformationAdded;
import static com.orangehrmlive.question.fill.WorkExperienceAdded.workExperienceAdded;
import static com.orangehrmlive.task.browse.BrowseToQualificationsSection.browseToQualificationsSection;
import static com.orangehrmlive.task.fill.FillLogIn.fillLogIn;
import static com.orangehrmlive.task.fill.FillQualificationsSkillAndLanguage.fillQualificationsSkillAndLanguage;
import static com.orangehrmlive.task.fill.FillQualificationsWorkAndEducation.fillQualificationsWorkAndEducation;
import static com.orangehrmlive.util.Dictionary.*;
import static com.orangehrmlive.util.GeneralData.qualificationsModelData;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.CoreMatchers.equalTo;

public class ManageMyInfoQualificationsTestStepDefinition extends SetUp {

    private static final Logger LOGGER = Logger.getLogger(ManageMyInfoQualificationsTestStepDefinition.class);
    private static final QualificationsModel qualificationsModel = qualificationsModelData();

    @When("I login and enter to the Qualifications section")
    public void iLoginAndEnterToTheQualificationsSection() {

        try{
            theActorInTheSpotlight().attemptsTo(
                    fillLogIn()
                            .withUserName(USER)
                            .andPassword(PASS),

                    browseToQualificationsSection()
            );

        }catch (Exception exception){
            LOGGER.error(exception.getMessage(), exception);
        }
    }

    @When("I add Work Experience and Education information and save it")
    public void iAddWorkExperienceAndEducationInformationAndSaveIt() {

        try{
            theActorInTheSpotlight().attemptsTo(
                    fillQualificationsWorkAndEducation()
                            .typingTheCompanyName(qualificationsModel.getCompanyNameModel())
                            .typingTheJobAtCompany(qualificationsModel.getJobAtCompanyModel())
                            .selectingTheMonthExperienceFrom(qualificationsModel.getMonthExperienceFromModel())
                            .selectingTheYearExperienceFrom(qualificationsModel.getYearExperienceFromModel())
                            .selectingTheMonthExperienceTo(qualificationsModel.getMonthExperienceToModel())
                            .selectingTheYearExperienceTo(qualificationsModel.getYearExperienceToModel())
                            .typingTheExperienceComment(qualificationsModel.getExperienceCommentModel())

                            .selectingTheEducationLevel(qualificationsModel.getEducationLevelModel())
                            .typingTheInstituteName(qualificationsModel.getInstituteModel())
                            .typingTheSpecializationName(qualificationsModel.getSpecializationModel())
                            .typingTheYearEducation(qualificationsModel.getYearEducationModel())
                            .typingTheEducationScore(qualificationsModel.getEducationScoreModel())
                            .selectingTheMonthEducationStart(qualificationsModel.getMonthEducationStartModel())
                            .selectingTheYearEducationStart(qualificationsModel.getYearEducationStartModel())
                            .selectingTheMonthEducationEnd(qualificationsModel.getMonthEducationEndModel())
                            .selectingTheYearEducationEnd(qualificationsModel.getYearEducationEndModel())
            );

        }catch (Exception exception){
            LOGGER.error(exception.getMessage(), exception);
        }

    }

    @When("I add Skill and Language information and save it")
    public void iAddSkillAndLanguageInformationAndSaveIt() {

        try{
            theActorInTheSpotlight().attemptsTo(
                    fillQualificationsSkillAndLanguage()
                            .selectingTheSkill(qualificationsModel.getSkillModel())
                            .typingTheYearsOfExperience(qualificationsModel.getYearsOfExperienceModel())
                            .typingTheSkillComment(qualificationsModel.getSkillCommentModel())

                            .selectingTheLanguage(qualificationsModel.getLanguageModel())
                            .selectingTheLanguageFluency(qualificationsModel.getLanguageFluencyModel())
                            .selectingTheLanguageCompetency(qualificationsModel.getLanguageCompetencyModel())
                            .typingTheLanguageComment(qualificationsModel.getLanguageCommentModel())
            );

        }catch (Exception exception){
            LOGGER.error(exception.getMessage(), exception);
        }

    }

    @Then("All saved information should appear in the Qualifications Section")
    public void allSavedInformationShouldAppearInTheQualificationsSection() {

        try {
            theActorInTheSpotlight().should(
                    seeThat(
                            workExperienceAdded()
                                    .wasValidatedWithCompanyName(qualificationsModel.getCompanyNameModel())
                                    .is(), equalTo(true)
                    ),
                    seeThat(
                            educationInformationAdded()
                                    .wasValidatedWithYearEducation(qualificationsModel.getYearEducationModel())
                                    .is(), equalTo(true)
                    ),
                    seeThat(
                            skillInformationAdded()
                                    .wasValidatedWithYearsOfExperience(qualificationsModel.getYearsOfExperienceModel())
                                    .is(), equalTo(true)
                    ),
                    seeThat(
                            languageInformationAdded()
                                    .wasValidatedWithCommentMessage(qualificationsModel.getLanguageCommentModel())
                                    .is(), equalTo(true)
                    )
            );
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
            LOGGER.warn(MANAGE_QUALIFICATIONS_VALIDATION_ERROR);
        }
        LOGGER.info(MANAGE_QUALIFICATIONS_DONE);
    }
}
