package in.reqres.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;


import static com.fasterxml.jackson.annotation.JsonInclude.Include;

public class GetResponseBase {

    @JsonInclude(Include.NON_DEFAULT)
    @JsonProperty("support")
    protected Support support;

    public Support getSupport() {
        return support;
    }

    public void setSupport(Support support) {
        this.support = support;
    }
}
