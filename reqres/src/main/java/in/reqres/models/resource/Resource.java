package in.reqres.models.resource;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import in.reqres.models.GetResponseBase;

import static com.fasterxml.jackson.annotation.JsonInclude.Include;

public class Resource extends GetResponseBase {
    @JsonInclude(Include.NON_DEFAULT)
    @JsonProperty("data")
    private DataResource dataResource;

    public DataResource getDataResource() {
        return dataResource;
    }

    public void setDataResource(DataResource dataResource) {
        this.dataResource = dataResource;
    }
}
