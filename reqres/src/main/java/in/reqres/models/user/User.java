package in.reqres.models.user;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import in.reqres.models.GetResponseBase;

import static com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonPropertyOrder({
        "data",
        "support"
})
public class User extends GetResponseBase {

    @JsonInclude(Include.NON_DEFAULT)
    @JsonProperty("data")
    public DataUser dataUser;

    public DataUser getDataUser() {
        return dataUser;
    }

    public void setDataUser(DataUser dataUser) {
        this.dataUser = dataUser;
    }
}
