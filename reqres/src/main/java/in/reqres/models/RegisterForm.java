package in.reqres.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import in.reqres.util.JsonConverter;

import static com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonPropertyOrder({
        "email",
        "password"
})
public class RegisterForm extends JsonConverter {
    @JsonInclude(Include.NON_DEFAULT)
    @JsonProperty("email")
    private String email;

    @JsonInclude(Include.NON_DEFAULT)
    @JsonProperty("password")
    private String password;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
