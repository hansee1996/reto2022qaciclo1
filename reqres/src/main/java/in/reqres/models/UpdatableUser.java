package in.reqres.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import in.reqres.util.JsonConverter;

import static com.fasterxml.jackson.annotation.JsonInclude.Include;
@JsonPropertyOrder({
        "name",
        "job",
        "id",
        "updatedAt",
        "createdAt"
})
public class UpdatableUser extends JsonConverter {
    @JsonInclude(Include.NON_DEFAULT)
    @JsonProperty("name")
    private String name;

    @JsonInclude(Include.NON_DEFAULT)
    @JsonProperty("job")
    private String job;

    @JsonInclude(Include.NON_DEFAULT)
    @JsonProperty("id")
    private String id;

    @JsonInclude(Include.NON_DEFAULT)
    @JsonProperty("updatedAt")
    private String updatedAt;

    @JsonInclude(Include.NON_DEFAULT)
    @JsonProperty("createdAt")
    private String createdAt;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

}
