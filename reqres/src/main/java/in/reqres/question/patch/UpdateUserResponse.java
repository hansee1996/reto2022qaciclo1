package in.reqres.question.patch;

import in.reqres.models.UpdatableUser;
import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

public class UpdateUserResponse implements Question<UpdatableUser> {
    public static UpdateUserResponse updateUser() {
        return new UpdateUserResponse();
    }

    @Override
    public UpdatableUser answeredBy(Actor actor) {
        return SerenityRest.lastResponse().as(UpdatableUser.class);
    }
}
