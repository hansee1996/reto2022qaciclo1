package in.reqres.question.put;

import in.reqres.models.UpdatableUser;
import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

public class ModifyUser implements Question<UpdatableUser> {

    public static ModifyUser modifyUser() {
        return new ModifyUser();
    }

    @Override
    public UpdatableUser answeredBy(Actor actor) {
        return SerenityRest.lastResponse().as(UpdatableUser.class);
    }
}
