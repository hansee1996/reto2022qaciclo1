package in.reqres.task.put;

import io.restassured.http.ContentType;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.rest.interactions.Put;

import java.nio.charset.StandardCharsets;

public class DoPutWithParam implements Task {
    private String resource;
    private String param;
    private String value;
    private String bodyRequest;

    public static DoPutWithParam doPutWithParams() {
        return new DoPutWithParam();
    }

    public DoPutWithParam usingTheResource(String resource) {
        this.resource = resource;
        return this;
    }

    public DoPutWithParam setParamAndValue(String param, String value) {
        this.param = param;
        this.value = value;
        return this;
    }

    public DoPutWithParam setBodyRequest(String bodyRequest) {
        this.bodyRequest = bodyRequest;
        return this;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Put.to(resource).with(
                        requestSpecification ->
                                requestSpecification.pathParam(param,value)
                                        .contentType(ContentType.JSON.withCharset(StandardCharsets.UTF_8))
                                        .body(bodyRequest)
                )
        );
    }
}
