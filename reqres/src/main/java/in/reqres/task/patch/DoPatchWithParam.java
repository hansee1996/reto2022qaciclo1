package in.reqres.task.patch;

import io.restassured.http.ContentType;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.rest.interactions.Patch;

import java.nio.charset.StandardCharsets;

public class DoPatchWithParam implements Task {
    private String resource;
    private String param;
    private String value;
    private String bodyRequest;

    public static DoPatchWithParam doPatchWithParam() {
        return new DoPatchWithParam();
    }

    public DoPatchWithParam usingTheResource(String resource) {
        this.resource = resource;
        return this;
    }

    public DoPatchWithParam setParamAndValue(String param, String value) {
        this.param = param;
        this.value = value;
        return this;
    }

    public DoPatchWithParam setBodyRequest(String bodyRequest) {
        this.bodyRequest = bodyRequest;
        return this;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Patch.to(resource).with(
                        requestSpecification ->
                                requestSpecification.pathParam(param,value)
                                        .contentType(ContentType.JSON.withCharset(StandardCharsets.UTF_8))
                                        .body(bodyRequest)
                )
        );
    }
}
