package in.reqres.task;

public enum OpeHttp {
    GET,
    POST,
    PUT,
    DELETE
}
