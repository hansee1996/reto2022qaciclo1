package in.reqres.task.post;


import io.restassured.http.ContentType;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.rest.interactions.Post;


import java.nio.charset.StandardCharsets;

public class DoPost implements Task {
    private String resource;
    private String bodyRequest;

    public static DoPost doPost() {
        return new DoPost();
    }
    public DoPost usingTheResource(String resource){
        this.resource = resource;
        return this;
    }

    public DoPost setBodyRequest(String bodyRequest) {
        this.bodyRequest = bodyRequest;
        return this;
    }


    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Post.to(resource)
                        .with(
                        requestSpecification ->
                                requestSpecification
                                        .contentType(ContentType.JSON.withCharset(StandardCharsets.UTF_8))
                                        .body(bodyRequest)
                )
        );
    }


}
