package in.reqres.task;

import com.mongodb.MongoException;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.rest.interactions.*;
import org.bson.Document;

import static com.mongodb.client.model.Filters.eq;

public class CommonTask implements Task {
    private static String resource;
    private static RestInteraction interaction;

    public static CommonTask peticion() {
        return new CommonTask();
    }



    public static RestInteraction withMethod(OpeHttp operacion) {
        switch (operacion) {
            case GET:
                interaction = Get.resource(resource);
                break;
            case POST:
                interaction = Post.to(resource);
                break;
            case PUT:
                interaction = Put.to(resource);
                break;
            case DELETE:
                interaction = Delete.from(resource);
                break;
        }
        return interaction;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                interaction
        );
    }
}
