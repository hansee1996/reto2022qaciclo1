package in.reqres.task.get;

import io.restassured.http.ContentType;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.rest.interactions.Get;

import java.nio.charset.StandardCharsets;

public class DoGetWithParam implements Task {

    private String resource;
    private String param;
    private String value;

    public static DoGetWithParam doGetWithParam() {
        return new DoGetWithParam();
    }

    public DoGetWithParam usingTheResource(String resource) {
        this.resource = resource;
        return this;
    }

    public DoGetWithParam setParamAndValue(String param, String value) {
        this.param = param;
        this.value = value;
        return this;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Get.resource(resource)
                        .with(
                        requestSpecification ->
                                requestSpecification.pathParam(param, value)
                                        .contentType(ContentType.JSON.withCharset(StandardCharsets.UTF_8))
                )
        );
    }

}

