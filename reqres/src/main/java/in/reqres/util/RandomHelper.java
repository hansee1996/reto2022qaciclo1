package in.reqres.util;

import com.github.javafaker.Faker;

public class RandomHelper {
    private static final int MIN_USER = 1;
    private static final int MAX_USER = 12;
    private static final int MAX_LIMIT_PARAM = 100;
    private static final Faker faker = new Faker();

    private RandomHelper() {
    }

    public static String randomUserId() {
        return String.valueOf(faker.number().numberBetween(MIN_USER, MAX_USER));
    }

    public static String randomNonExistUserId() {
        return String.valueOf(faker.number().numberBetween(MAX_USER + 1, MAX_LIMIT_PARAM));
    }

    public static String randomFirstName() {
        return faker.name().firstName();
    }

    public static String randomJob() {
        return faker.job().title();
    }

    public static String randomPassword() {
        return faker.internet().password();
    }

}
