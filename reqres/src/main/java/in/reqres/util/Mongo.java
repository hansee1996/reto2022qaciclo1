package in.reqres.util;

import com.mongodb.MongoException;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

public class Mongo {

    public static void main(String[] args) {

        String uri = "mongodb+srv://hansee:proyectos@proyectos.zugzk.mongodb.net/?retryWrites=true&w=majority";

        try (MongoClient mongoClient = MongoClients.create(uri)) {
            MongoDatabase db = mongoClient.getDatabase("BD_Proyectos");
            MongoCollection<Document> dbCollection = db.getCollection("usuarios");

//            db.createCollection("Holassss");
//            Document document = dbCollection.find(eq("_id", new ObjectId("61c92288ed3e1e1fcf24080c"))).first();
//            System.out.println(document.toJson());

//            for (Document document : dbCollection.find(eq("estado", "AUTORIZAD"))) {
//                System.out.println(document.toJson());
//            }

        } catch (MongoException e) {
            System.out.println(e);
        }

    }
}
