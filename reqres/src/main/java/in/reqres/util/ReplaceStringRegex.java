package in.reqres.util;

import java.util.regex.Pattern;

public interface ReplaceStringRegex {

    String replaceParam(String param);

    default String replaceString(String value,String param) {
        String regex = "\\{(\\w+)+}";
        return value.replaceFirst(Pattern.compile(regex, Pattern.CASE_INSENSITIVE).pattern(), param);
    }
}
