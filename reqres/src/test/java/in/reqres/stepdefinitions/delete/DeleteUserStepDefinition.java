package in.reqres.stepdefinitions.delete;

import in.reqres.stepdefinitions.setup.BaseResources;
import in.reqres.util.DeleteResources;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.junit.jupiter.api.Assertions;

import static in.reqres.question.CheckHttpResponse.checkHttpResponse;
import static in.reqres.question.StringResponse.stringResponse;
import static in.reqres.task.delete.DoDeleteWithParam.deleteUser;
import static in.reqres.util.RandomHelper.randomUserId;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.hamcrest.Matchers.isEmptyString;

public class DeleteUserStepDefinition extends BaseResources {

    private static final Logger LOGGER = Logger.getLogger(DeleteUserStepDefinition.class);
    private static final Actor admin = new Actor("Administrator");

    @Given("that you have access to the appropriate web resource")
    public void thatYouHaveAccessToTheAppropriateWebResource() {
        generalSetUp();
        try {
            admin.whoCan(CallAnApi.at(BASE_REQRES));
            LOGGER.info("Getting in the URI: ".concat(BASE_REQRES));
        } catch (Exception exception) {
            LOGGER.error("An error occurred while entering the base URI: ".concat(BASE_REQRES));
            Assertions.fail(exception);
        }
    }

    @When("you want to delete a user")
    public void youWantToDeleteAUser() {
        String userId = randomUserId();

        try {
            admin.attemptsTo(
                    deleteUser()
                            .usingTheResource(DeleteResources.DELETE_USER.getValue())
                            .setParamAndValue("id", userId)
            );

            LOGGER.info("Access to DELETE resource: ".concat(DeleteResources.DELETE_USER.replaceParam(userId)));

        } catch (Exception exception) {
            LOGGER.error("An error occurred while deleting a user");
            Assertions.fail(exception);
        }
    }

    @Then("results in a successfully deleted user")
    public void resultsInASuccessfullyDeletedUser() {
        checkHttpResponse().setExpectedResponse(HttpStatus.SC_NO_CONTENT).answeredBy(admin);
        admin.should(
                seeThat("the response message", stringResponse(), isEmptyString())
        );
    }

}
