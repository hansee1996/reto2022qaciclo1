package in.reqres.stepdefinitions.setup;

import org.apache.log4j.PropertyConfigurator;

import java.util.Locale;

import static in.reqres.util.Log4jValues.*;

public class BaseResources {
    protected static final String BASE_REQRES = "https://reqres.in";

    protected void generalSetUp() {
        setUpLog4j2();
    }

    private void setUpLog4j2() {
        String os = System.getProperty("os.name").toLowerCase(Locale.ROOT).substring(0, 3);
        switch (os) {
            case "win":
                PropertyConfigurator.configure(USER_DIR.getValue().concat(LOG4J_PROPERTIES_FILE_PATH.getValue()));
                break;

            case "lin":
                PropertyConfigurator.configure(USER_DIR.getValue().concat(LOG4J_LINUX_PROPERTIES_FILE_PATH.getValue()));
                break;
        }
    }

}






