package in.reqres.stepdefinitions.post;

import in.reqres.models.RegisterForm;
import in.reqres.stepdefinitions.setup.BaseResources;

import static in.reqres.question.CheckHttpResponse.checkHttpResponse;
import static in.reqres.question.StringResponse.stringResponse;
import static in.reqres.task.post.DoPost.doPost;
import static in.reqres.util.RandomHelper.randomPassword;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;
import static org.hamcrest.CoreMatchers.*;

import in.reqres.util.PostResources;
import in.reqres.util.RegistrationUserEmail;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.junit.jupiter.api.Assertions;


public class RegisterUserStepDefinition extends BaseResources {

    private static final Logger LOGGER = Logger.getLogger(RegisterUserStepDefinition.class);
    private final Actor actor = Actor.named("Usuario");

    @Given("the client enters on the page")
    public void theClientEntersOnThePage() {
        generalSetUp();
        try {
            actor.whoCan(CallAnApi.at(BASE_REQRES));
            LOGGER.info("Getting in the URI: ".concat(BASE_REQRES));
        } catch (Exception exception) {
            LOGGER.error("An error occurred while entering the base URI: ".concat(BASE_REQRES));
            Assertions.fail(exception);
        }
    }

    @When("creates a new user")
    public void createsANewUser() {
        RegisterForm registerForm = new RegisterForm();

        registerForm.setEmail(RegistrationUserEmail.USER_ONE.getValue());
        registerForm.setPassword(randomPassword());
        try {
            actor.attemptsTo(
                    doPost()
                            .usingTheResource(PostResources.REGISTER_USER.getValue())
                            .setBodyRequest(registerForm.toJson())
            );
            LOGGER.info("Successful access to POST resource:".concat(PostResources.REGISTER_USER.getValue()));
            LOGGER.info("User data to register:\n" + registerForm.toJson());
        } catch (Exception exception) {
            LOGGER.error("An error occurred while getting POST resource:".concat(PostResources.REGISTER_USER.getValue()));
            Assertions.fail(exception);
        }
    }

    @Then("you will get an authentication token")
    public void youWillGetAnAuthenticationToken() {
        checkHttpResponse().setExpectedResponse(HttpStatus.SC_OK).answeredBy(actor);
        String response = stringResponse().answeredBy(actor);

        actor.should(
                seeThat("the response has an id",
                        responseRequest -> response, containsString("id")
                ),
                seeThat("the response has a token",
                        responseRequest -> response, containsString("token")
                )
        );
    }

    @When("create a user without password")
    public void createAUserWithoutPassword() {
        RegisterForm registerForm = new RegisterForm();
        registerForm.setEmail(RegistrationUserEmail.USER_ONE.getValue());
        try {

            actor.attemptsTo(
                    doPost()
                            .usingTheResource(PostResources.REGISTER_USER.getValue())
                            .setBodyRequest(registerForm.toJson())

            );
            LOGGER.info("Successful access to POST resource:".concat(PostResources.REGISTER_USER.getValue()));
            LOGGER.info("User data to register:\n" + registerForm.toJson());
        } catch (Exception exception) {
            LOGGER.error("An error occurred while getting POST resource:".concat(PostResources.REGISTER_USER.getValue()));
            Assertions.fail(exception);
        }
    }

    @Then("he will get an error message")
    public void heWillGetAnErrorMessage() {
        checkHttpResponse().setExpectedResponse(HttpStatus.SC_BAD_REQUEST).answeredBy(actor);

        actor.should(
                seeThatResponse("shows an error",
                        validateResponse -> validateResponse.body("error", equalTo("Missing password")))
        );

    }
}
