package in.reqres.runners.get;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith (CucumberWithSerenity.class)
@CucumberOptions(
        snippets = CucumberOptions.SnippetType.CAMELCASE,
        features = {"src/test/resources/features/reqres/get/viewUser.feature"},
        glue = "in.reqres.stepdefinitions.get",
        plugin = {"pretty", "html:target/cucumber-reports"}
)
public class ViewUserRunner {
}