# new feature
# Tags: optional

Feature: Modify user data
  As an user of ReqRes
  I want to be able to modify my personal information
  To keep my data up to date

  Scenario: Successfully data modified
    Given the user has access to the appropriate web resource
    When the user want to modify his data
    Then the user can see his modified information