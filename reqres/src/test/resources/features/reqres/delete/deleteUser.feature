# new feature
# Tags: optional

Feature: Remove User From System
  As Administrator of the ReqRes website
  I want to delete an user
  To keep the database clean


  Scenario: Successfully deleted user
    Given that you have access to the appropriate web resource
    When you want to delete a user
    Then results in a successfully deleted user