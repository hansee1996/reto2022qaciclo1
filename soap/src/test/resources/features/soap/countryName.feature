Feature: Country name
  As a geologist
  I need to convert country ISO codes into a formal language
  to fasten the comprehensive process with clients

  Scenario: Convert from country ISO to formal name
    Given that the user is in the web resource giving the country ISO name "COL"
    When the user generates the query
    Then the user will see the formal name of the country as "Colombia"

  Scenario: Convert from country ISO to formal name with fake ISO code
    Given that the user is in the web resource giving the something similar to an ISO name like "HOLA"
    When the user generates the query
    Then the user will see that there is no country name for that code in the database
