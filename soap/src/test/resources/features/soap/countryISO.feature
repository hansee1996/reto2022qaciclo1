# new feature

  Feature: country ISO code
    as a user of the website
    i need to know the ISO code of a country
    so i can know its standard abbreviation

    Scenario: get country ISO code
      Given the user is on the website with the country "Colombia"
      When the user make the request
      Then the website returns the ISO code "CO"

    Scenario: failed to get country ISO code
      Given the user is on the website with the country "Paris"
      When the user make the request
      Then the website returns the message "No country found by that name"
