package webservices.org.oorsprong.stepdefinition.countryisocode;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import webservices.org.oorsprong.stepdefinition.SetupLog4j2;

import java.util.HashMap;
import java.util.Map;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;
import static org.hamcrest.CoreMatchers.containsString;
import static webservices.org.oorsprong.question.countrybyname.SoapResponse.response;
import static webservices.org.oorsprong.task.countrybyname.DoPost.doPost;
import static webservices.org.oorsprong.util.Dictionary.*;
import static webservices.org.oorsprong.util.Utilities.defineBodyRequest;
import static webservices.org.oorsprong.util.countryNameKey.COUNTRY_NAME;

public class CountryISOCodeStepDefinition extends SetupLog4j2 {
    private static final Logger LOGGER = Logger.getLogger(CountryISOCodeStepDefinition.class);

    private final Map<String, Object> headers = new HashMap<>();
    private final Actor actor = Actor.named("JuanManuel");
    private String bodyRequest;

    @Given("the user is on the website with the country {string}")
    public void theUserIsOnTheWebsiteWithTheCountry(String country) {
        setup();
        actor.can(CallAnApi.at(URL_BASE));
        headers.put("Content-Type", "application/soap+xml;charset=UTF-8");
        headers.put(SOAP_ACTION, "");
        bodyRequest = defineBodyRequest(COUNTRY_NAME_XML_LINUX_PATH, COUNTRY_NAME.getValue(), country);
        LOGGER.info(bodyRequest);
    }

    @When("the user make the request")
    public void theUserMakeTheRequest() {
        actor.attemptsTo(
                doPost()
                        .usingTheResource(RESOURCE)
                        .withHeaders(headers)
                        .andBodyRequest(bodyRequest)
        );
    }

    @Then("the website returns the ISO code {string}")
    public void theWebsiteReturnsTheISOCode(String ISOCode) {
        actor.should(
                seeThatResponse("El código de respuesta debe ser " + HttpStatus.SC_OK,
                        validatableResponse -> validatableResponse.statusCode(HttpStatus.SC_OK)
                ),
                seeThat("El código ISO del país debe ser " + ISOCode,
                        response(), containsString(ISOCode))
        );
    }

    @Then("the website returns the message {string}")
    public void theWebsiteReturnsTheMessage(String errorMessage) {
        actor.should(
                seeThatResponse("El código de respuesta debe ser " + HttpStatus.SC_OK,
                        validatableResponse -> validatableResponse.statusCode(HttpStatus.SC_OK)
                ),
                seeThat("El mensaje de error debe ser " + errorMessage,
                        response(), containsString(errorMessage))
        );
    }
}
