package webservices.org.oorsprong.runners;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
    snippets = CucumberOptions.SnippetType.CAMELCASE,
        features = {"src/test/resources/features/soap/FlagFileByISOCountryCode.feature" },
        glue = {"webservices.org.oorsprong.stepdefinition.countryflag"},
        tags = {""}
            )

public class CountryFlag {
}
