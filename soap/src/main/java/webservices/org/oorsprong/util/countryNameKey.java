package webservices.org.oorsprong.util;

public enum countryNameKey {
    COUNTRY_NAME("[countryName]");

    private final String value;

    countryNameKey(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
