package webservices.org.oorsprong.util;

public enum CountryISOCodeKey {
    COUNTRY_ISO_CODE("[countryISOCode]");

    private final String value;

    CountryISOCodeKey(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
